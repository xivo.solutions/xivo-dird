# xivo/xivo-dird (see Makefile)

FROM python:3.11-slim-bookworm

RUN apt-get -yq update \
   && apt-get -yqq dist-upgrade \
   && apt-get -yqq install \
      libldap2-dev \
      libsasl2-dev \
      libssl-dev \
      git \
      gcc \
   && apt-get -yq autoremove

RUN mkdir -p /etc/xivo-dird/conf.d

RUN mkdir -p /var/run/xivo-dird
RUN chmod a+w /var/run/xivo-dird

RUN touch /var/log/xivo-dird.log
RUN chown www-data: /var/log/xivo-dird.log

WORKDIR /usr/src/xivo-dird
# First copy requirements only to use pip cache if --no-cache-dir option is not used
COPY requirements.txt .
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --no-cache-dir -r requirements.txt

ADD ./contribs/docker/certs /usr/share/xivo-certs
ADD . /usr/src/xivo-dird
RUN cp -r etc/* /etc

RUN python3 setup.py install
# RUN rm -rf /usr/src/xivo-dird

ONBUILD ADD ./contribs/docker/certs /usr/share/xivo-certs

EXPOSE 9489

CMD ["xivo-dird", "-fd"]
