# -*- coding: utf-8 -*-

# Copyright (C) 2014-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging

from hamcrest import assert_that, contains_exactly
from .base_dird_integration_test import BaseDirdIntegrationTest

logger = logging.getLogger(__name__)

persons = (
    {'firstname': 'Violet', 'lastname': 'Caterpillar', 'name': 'Violet Caterpillar'},
    {'firstname': 'Polecat', 'lastname': 'Striped', 'name': 'Polecat Striped'},
    {'firstname': 'Nice', 'lastname': 'Cute', 'company': 'Cat', 'name': 'Nice Cute'},
    {'firstname': 'Cat', 'lastname': 'White', 'name': 'Cat White'},
    {'firstname': 'black', 'lastname': 'Cat', 'name': 'black Cat'},
    {'firstname': 'Zorro', 'lastname': 'Cat', 'name': 'Zorro Cat'},
    {'firstname': 'Cat', 'lastname': 'Whíne', 'name': 'Cat Whíne'},
)

class TestSearchSorting(BaseDirdIntegrationTest):

    asset = 'search_sort'
    uuid = "6fa459ea-ee8a-3ca4-894e-db77e160355e"

    def test_that_the_lookup_returns_properly_sorted(self):
        for person in persons:
            self.post_personal(person)

        expected_results = [
            ['black Cat', None, None, False, 'black', 'Cat'],
            ['Cat Whíne', None, None, False, 'Cat', 'Whíne'],
            ['Cat White', None, None, False, 'Cat', 'White'],
            ['Zorro Cat', None, None, False, 'Zorro', 'Cat'],
            ['Violet Caterpillar', None, None, False, 'Violet', 'Caterpillar'],
            ['Polecat Striped', None, None, False, 'Polecat', 'Striped'],
            ['Bat', 'Bat', '1011', False, None, None]
        ]
        result = list([_['column_values'] for _ in self.lookup('cat', 'default')['results']])
        assert_that(result, contains_exactly(*expected_results))
