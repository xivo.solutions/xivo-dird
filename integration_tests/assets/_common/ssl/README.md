How to generate certificates
============================

Create self-signed certificate for 10 years:

openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -config openssl.cfg -keyout server.key -out server.crt