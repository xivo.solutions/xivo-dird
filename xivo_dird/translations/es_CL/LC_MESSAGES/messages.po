# Translations template for xivo-dird.
# Copyright (C) 2015 Avencall
# This file is distributed under the same license as the xivo-dird project.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: XiVO\n"
"Report-Msgid-Bugs-To: dev@avencall.com\n"
"POT-Creation-Date: 2015-10-06 09:46-0400\n"
"PO-Revision-Date: 2015-11-16 18:58+0000\n"
"Last-Translator: XiVO Dev Team <dev@avencall.com>\n"
"Language-Team: Spanish (Chile) (http://www.transifex.com/avencall/xivo/language/es_CL/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 1.3\n"
"Language: es_CL\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: xivo_dird/plugins/templates/aastra_input.jinja:3
#: xivo_dird/plugins/templates/cisco_input.jinja:3
#: xivo_dird/plugins/templates/polycom_input.jinja:4
#: xivo_dird/plugins/templates/polycom_results.jinja:4
#: xivo_dird/plugins/templates/snom_input.jinja:3
msgid "XiVO Search"
msgstr ""

#: xivo_dird/plugins/templates/aastra_input.jinja:4
#: xivo_dird/plugins/templates/polycom_input.jinja:9
#: xivo_dird/plugins/templates/snom_input.jinja:4
msgid "Name or number:"
msgstr ""

#: xivo_dird/plugins/templates/aastra_results.jinja:5
#: xivo_dird/plugins/templates/polycom_results.jinja:9
msgid "Previous Page"
msgstr ""

#: xivo_dird/plugins/templates/aastra_results.jinja:17
#: xivo_dird/plugins/templates/cisco_results.jinja:10
#: xivo_dird/plugins/templates/polycom_results.jinja:15
#: xivo_dird/plugins/templates/snom_results.jinja:10
#: xivo_dird/plugins/templates/thomson_results.jinja:10
#: xivo_dird/plugins/templates/yealink_results.jinja:10
msgid "No entries"
msgstr ""

#: xivo_dird/plugins/templates/aastra_results.jinja:24
#: xivo_dird/plugins/templates/polycom_results.jinja:19
msgid "Next Page"
msgstr ""

#: xivo_dird/plugins/templates/cisco_input.jinja:4
#: xivo_dird/plugins/templates/cisco_input.jinja:7
#: xivo_dird/plugins/templates/snom_input.jinja:7
msgid "Name or number"
msgstr ""

#: xivo_dird/plugins/templates/cisco_menu.jinja:4
msgid "XiVO Directory"
msgstr ""

#: xivo_dird/plugins/templates/cisco_results.jinja:17
msgid "Dial"
msgstr ""

#: xivo_dird/plugins/templates/cisco_results.jinja:24
#: xivo_dird/plugins/templates/snom_results.jinja:17
msgid "PrevPage"
msgstr ""

#: xivo_dird/plugins/templates/cisco_results.jinja:31
msgid "Exit"
msgstr ""

#: xivo_dird/plugins/templates/cisco_results.jinja:38
#: xivo_dird/plugins/templates/snom_results.jinja:25
msgid "NextPage"
msgstr ""

#: xivo_dird/plugins/templates/polycom_input.jinja:13
msgid "Search"
msgstr ""
