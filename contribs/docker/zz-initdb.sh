#!/bin/bash
set -e

#Allow local connection on default docker network
echo "host  asterisk    all 0.0.0.0/0  md5" >> /var/lib/postgresql/15/data/pg_hba.conf
echo "host  postgres    all 0.0.0.0/0  md5" >> /var/lib/postgresql/15/data/pg_hba.conf

xivo-dird-init-db
echo "Upgrading xivo-dird database ..."
cd /usr/src/xivo-dird && alembic -c alembic.ini upgrade head

# Add some meetingroom for tests
if [ "${INIT_DB_TEST}" = "true" ] ; then
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "asterisk" <<-EOSQL
        INSERT INTO "meetingroom" (id, display_name, number, room_type)
        VALUES
            (1, 'room', '1001', 'static'),
            (2, 'my meeting', '1002', 'static'),
            (3, 'Bat', '1011', 'static'),
            (4, 'Bobby Bat', '1012', 'static');

EOSQL

fi
